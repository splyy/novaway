<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Movie;
use AppBundle\Services\GetResult;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/", name="home")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $books = $em->getRepository('AppBundle:Book')->getLastBook();
        $movies = $em->getRepository('AppBundle:Movie')->getLastsMovie();

        return $this->render('public/index.html.twig', [
            'movies' => $movies,
            'books' => $books
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/book", name="listBook")
     */
    public function listBook()
    {
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository('AppBundle:Book')->findAll();

        return $this->render('public/listBook.html.twig', [
            'books' => $books,
        ]);
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/movie", name="listMovie")
     */
    public function listMovie()
    {
        $em = $this->getDoctrine()->getManager();
        $movies = $em->getRepository('AppBundle:Movie')->findAll();

        return $this->render('public/listMovie.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/book/{id}", name="showBook")
     */
    public function showBookAction(Book $book)
    {
        return $this->render('public/book.html.twig', [
            'data' => $book
        ]);
    }

    /**
     * @param Movie $movie
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/movie/{id}", name="showMovie")
     */
    public function showMovieAction(Movie $movie)
    {
        return $this->render('public/movie.html.twig', [
            'data' => $movie
        ]);
    }

    /**
     * @param Request $request
     * @param GetResult $service
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/result", name="search")
     */
    public function formSearch(Request $request, GetResult $service)
    {
        $sql = urldecode($request->query->get("sql"));
        $all = $service->search($sql);
        dump($all);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $all,
            $request->query->get('page', 1),
            5
        );

        return $this->render('public/search.html.twig', array(
            'all' => $pagination,
        ));
    }
}
