<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Movie;
use AppBundle\Form\BookType;
use AppBundle\Form\DeleteBookType;
use AppBundle\Form\DeleteMovieType;
use AppBundle\Form\MovieType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin", name="admin")
     */
    public function adminAction()
    {
        $em = $this->getDoctrine()->getManager();

        $movies = $em->getRepository('AppBundle:Movie')->findAll();
        $books = $em->getRepository('AppBundle:Book')->findAll();

        // IF submit

        return $this->render('admin/admin.html.twig', [
            'movies' => $movies,
            'books' => $books
        ]);
    }

    /**
     * @Route("/admin/add/{type}", name="addBook")
     * @param Request $request
     * @param $type
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function addAdmin(Request $request, $type)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $new = "";

        if ($type == "book") {
            $new = new Book();
            $form = $this->createForm(BookType::class, $new);
        } elseif ($type == "movie") {
            $new = new Movie();
            $form = $this->createForm(MovieType::class, $new);
        }

        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {
            $em->persist($new);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/add.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Book $book
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/book/{id}", name="editBook")
     */
    public function editBookAdmin(Request $request, Book $book)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(BookType::class, $book);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        $formDelete = $this->createForm(DeleteBookType::class, $book);
        $formDelete->handleRequest($request);

        if ($formDelete->isValid() && $formDelete->isSubmitted()) {
            $em->remove($book);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView(),
            'formDelete' => $formDelete->createView(),
        ]);
    }

    /**
     * @param Request $request
     * @param Movie $movie
     * @return \Symfony\Component\HttpFoundation\Response
     * @Route("/admin/movie/{id}", name="editMovie")
     */
    public function editMovieAdmin(Request $request, Movie $movie)
    {
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(MovieType::class, $movie);
        $form->handleRequest($request);

        if ($form->isValid() && $form->isSubmitted()) {
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        $formDelete = $this->createForm(DeleteMovieType::class, $movie);
        $formDelete->handleRequest($request);

        if ($formDelete->isValid() && $formDelete->isSubmitted()) {
            $em->remove($movie);
            $em->flush();
            return $this->redirectToRoute('admin');
        }

        return $this->render('admin/edit.html.twig', [
            'form' => $form->createView(),
            'formDelete' => $formDelete->createView(),
        ]);
    }
}
