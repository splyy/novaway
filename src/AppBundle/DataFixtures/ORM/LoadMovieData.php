<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Movie;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Provider\DateTime;

class LoadMovieData extends AbstractFixture implements OrderedFixtureInterface
{
    const MAX_MOVIE = 3;

    /**
     * Load data fixtures with the passed EntityManager
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        for ($i = 0; $i < self::MAX_MOVIE; $i++) {
            $movie = new Movie();
            $movie->setType("DVD");
            $movie->setIsan($faker->bothify('####-####-####-####-?-####-####-?'));
            $movie->setTitle($faker->title);
            $movie->setDirector($faker->name);
            $movie->setDate(new \DateTime());
            $movie->setDuring($faker->numberBetween(1,500));
            $movie->setSummary($faker->sentences(3, true));
            $movie->setPrice($faker->randomFloat(2, 0, 1000));
            $movie->setActors($faker->name.", ".$faker->name);

            $manager->persist($movie);
        }

        for ($i = 0; $i < self::MAX_MOVIE; $i++) {
            $movie = new Movie();
            $movie->setType("Blu-Ray");
            $movie->setIsan($faker->bothify('####-####-####-####-?-####-####-?'));
            $movie->setTitle($faker->title);
            $movie->setDirector($faker->name);
            $movie->setDate(new \DateTime());
            $movie->setDuring($faker->numberBetween(1,500));
            $movie->setSummary($faker->sentences(3, true));
            $movie->setPrice($faker->randomFloat(2, 0, 1000));
            $movie->setActors($faker->name.", ".$faker->name);

            $manager->persist($movie);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}