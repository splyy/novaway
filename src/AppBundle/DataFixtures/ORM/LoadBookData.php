<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Book;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class LoadBookData extends AbstractFixture implements OrderedFixtureInterface
{
    const MAX_BOOK = 5;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i = 0; $i < self::MAX_BOOK; $i++) {
            $book = new Book();
            $book->setIsnb($faker->bothify('#-######-##-#'));
            $book->setTitle($faker->title);
            $book->setAuthor($faker->name);
            $book->setDate(new \DateTime());
            $book->setPage($faker->numberBetween(1,500));
            $book->setSummary($faker->sentences(3, true));
            $book->setPrice($faker->randomFloat(2, 0, 1000));

            $manager->persist($book);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 1;
    }
}