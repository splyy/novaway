<?php

namespace AppBundle\Services;

use Doctrine\ORM\EntityManager;

/**
* @package AppBundle\Services
*/
class GetResult
{
    private $em;

    /**
     * GetResult constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @param string $search
     * @return array
     */
    public function search($search){

        $books  = $this->em->getRepository('AppBundle:Book')->searchBook($search);
        $movies  = $this->em->getRepository('AppBundle:Movie')->searchMovie($search);

        $all = array_merge($books, $movies);
        ksort($all);

        return $all;
    }
}