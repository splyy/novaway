<?php

namespace AppBundle\Form;

use AppBundle\Entity\Book;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isnb', TextType::class, [
                'label' => 'Numéro ISNB',
                'attr' => [
                    'placeholder' => '#-######-##-#'
                ]
            ])
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => 'Ex: Eragon'
                ]
            ])
            ->add('author', TextType::class, [
                'label' => 'Auteur',
                'attr' => [
                    'placeholder' => 'Ex: Christopher Paolini'
                ]
            ])
            ->add('date', DateType::class, [ 'label' => 'Date de publication' ])
            ->add('page', NumberType::class, [
                'label' => 'Nombre de pages',
                'attr' => [ 'placeholder' => '350 pages' ]
            ])
            ->add('summary', TextareaType::class, [
                'label' => 'Résumé',
                'attr' => [ 'placeholder' => 'Il était une fois ...' ]
            ])
            ->add('price', NumberType::class, [
                'label' => 'Prix',
                'attr' => [ 'placeholder' => '25.99€' ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Book::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_book_type';
    }
}
