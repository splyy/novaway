<?php

namespace AppBundle\Form;

use AppBundle\Entity\Movie;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MovieType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isan', TextType::class, [
                'label' => 'Numéro ISAN',
                'attr' => [
                    'placeholder' => '####-####-####-####-?-####-####-?'
                ]
            ])
            ->add('title', TextType::class, [
                'label' => 'Titre',
                'attr' => [
                    'placeholder' => 'Ex: Le Seigneur des Anneaux'
                ]
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Type',
                'choices' => [
                    'DVD' => 'DVD',
                    'Blu-Ray' => 'Blu-Ray'
                ]
            ])
            ->add('director', TextType::class, [
                'label' => 'Réalisateur',
                'attr' => [
                    'placeholder' => 'Ex: Peter Jackson'
                ]
            ])
            ->add('actors', TextType::class, [
                'label' => 'Acteur(s)',
                'attr' => [
                    'placeholder' => 'Orlando Bloom, Elijah Wood ...'
                ]
            ])
            ->add('date', DateType::class, [ 'label' => 'Date de sortie' ])
            ->add('during', NumberType::class, [
                'label' => 'Durée (en min)',
                'attr' => [ 'placeholder' => '120min' ]
            ])
            ->add('summary', TextareaType::class, [
                'label' => 'Résumé',
                'attr' => [ 'placeholder' => 'Il était une fois ...' ]
            ])
            ->add('price', NumberType::class, [
                'label' => 'Prix',
                'attr' => [ 'placeholder' => '25.99€' ]
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Movie::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'app_bundle_movie_type';
    }
}
