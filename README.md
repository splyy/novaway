NATest
======

Project Symfony for Novaway Test.

Get Started
======

Cloner le dépôt en local
```
git clone https://splyy@bitbucket.org/splyy/novaway.git
```

Se placer dans le dossier cloner
```
cd novaway/
```

Mettre en place le vendor/
```
composer install
```

Configurer si nécessaire parameters.yml
```
app/parameters.yml
```

Créer la BDD
```
bin/console doctrine:database:create
```

Ajouter les tables à la BDD
```
bin/console doctrine:schema:update --force
```

Lancer le serveur
```
bin/console server:run
```
```
bin/console server:start
```

Remplissage de la BDD avec des données quelconques 
```
bin/console doctrine:fixtures:load
```

FRONT-OFFICE
======
/^
```
/ : Page d'accueil avec les 5 derniers ajouts de Livre et de Filmm ainsi qu'un formulaire de recherche
/book, /movie : Listing sous forme de tableau de tous les livres, films
/result : résultat de la recherche effectué
```

BACK-OFFICE
======
/admin : Accès au panel administrateur listant toutes la data
```
Login : admin
Mdp : admin
```

/admin/^
```
* Ajout
* Modification
* Suppression
```